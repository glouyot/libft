/* ************************************************************************** */
/*                                                                            */
/*                   ::::::::::  ::::::::  :::::::::  ::::::::::              */
/*                  :+:        :+:    :+: :+:    :+: :+:    :+:               */
/*                 +:+              +:+  +:+    +:+ +:+                       */
/*                +#++:++#       +#+    +#++:++#:  +#++:++#+                  */
/*               +#+          +#+      +#+    +#+        +#+                  */
/*              #+#         #+#       #+#    #+# #+#    #+#                   */
/*             ########## ########## ###    ###  ########                     */
/*                                                                            */
/*                ft_tolower.c                                                */
/*                                                                            */
/*                By: glouyot <glouyot@protonmail.com>                        */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_tolower(int c)
{
	return ((c >= 65 && c <= 90) ? c + 32 : c);
}
