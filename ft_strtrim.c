/* ************************************************************************** */
/*                                                                            */
/*                   ::::::::::  ::::::::  :::::::::  ::::::::::              */
/*                  :+:        :+:    :+: :+:    :+: :+:    :+:               */
/*                 +:+              +:+  +:+    +:+ +:+                       */
/*                +#++:++#       +#+    +#++:++#:  +#++:++#+                  */
/*               +#+          +#+      +#+    +#+        +#+                  */
/*              #+#         #+#       #+#    #+# #+#    #+#                   */
/*             ########## ########## ###    ###  ########                     */
/*                                                                            */
/*                ft_strtrim.c                                                */
/*                                                                            */
/*                By: glouyot <glouyot@protonmail.com>                        */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char		*ft_strtrim(char const *s)
{
	char	*dst;
	size_t	i;
	size_t	a;

	if (!s)
		return (NULL);
	a = 0;
	i = ft_strlen(s);
	while (ft_isspace(s[a]))
		a++;
	while (ft_isspace(s[i - 1]))
		i--;
	if (i < a)
		i = a;
	if (!(dst = ft_strnew(i - a)))
		return (NULL);
	return (ft_strncpy(dst, s + a, i - a));
}
