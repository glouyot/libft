/* ************************************************************************** */
/*                                                                            */
/*                   ::::::::::  ::::::::  :::::::::  ::::::::::              */
/*                  :+:        :+:    :+: :+:    :+: :+:    :+:               */
/*                 +:+              +:+  +:+    +:+ +:+                       */
/*                +#++:++#       +#+    +#++:++#:  +#++:++#+                  */
/*               +#+          +#+      +#+    +#+        +#+                  */
/*              #+#         #+#       #+#    #+# #+#    #+#                   */
/*             ########## ########## ###    ###  ########                     */
/*                                                                            */
/*                tests_project.h                                             */
/*                                                                            */
/*                By: glouyot <glouyot@protonmail.com>                        */
/*                                                                            */
/* ************************************************************************** */

/* spécifique a chaque projet liste des tests unitaires du project
*  sous la forme int nom_test(void)
*/

#ifndef TESTS_PROJECT_H
# define TESTS_PROJECT_H

int atoi_launcher(void);
int positif_test(void);
int negatif_test(void);
int int_max_test(void);
int int_min_test(void);

#endif