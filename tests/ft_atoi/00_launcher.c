/* ************************************************************************** */
/*                                                                            */
/*                   ::::::::::  ::::::::  :::::::::  ::::::::::              */
/*                  :+:        :+:    :+: :+:    :+: :+:    :+:               */
/*                 +:+              +:+  +:+    +:+ +:+                       */
/*                +#++:++#       +#+    +#++:++#:  +#++:++#+                  */
/*               +#+          +#+      +#+    +#+        +#+                  */
/*              #+#         #+#       #+#    #+# #+#    #+#                   */
/*             ########## ########## ###    ###  ########                     */
/*                                                                            */
/*                00_launcher.c                                               */
/*                                                                            */
/*                By: glouyot <glouyot@protonmail.com>                        */
/*                                                                            */
/* ************************************************************************** */

#include "libunit.h"
#include "../tests_project.h"

int	atoi_launcher(void)
{
	t_unit_test	*testlist;

	print(1, "ATOI:\n", 6);
	testlist = NULL;
	load_test(&testlist, "positif test", &positif_test,
			create_signal_code(0, 0));
	load_test(&testlist, "negatif test", &negatif_test,
			create_signal_code(0, 0));
	load_test(&testlist, "int max test", &int_max_test,
			create_signal_code(0, 0));
	load_test(&testlist, "int min test", &int_max_test,
			create_signal_code(0, 0));
	return (launch_test(&testlist));
}